from sklearn.neighbors import NearestCentroid
from .classifier import Classifier


class NearestCentroidClassifier(Classifier):

    def __init__(self, metric='euclidean', shrink_threshold=None):
        self.classifier = NearestCentroid(
            metric=metric,
            shrink_threshold=shrink_threshold
        )

    def get_method_name(self):
        return 'nearest centroid'

    def create_model(self, samples, classes):
        self.classifier.fit(samples, classes)

    def predict(self, samples):
        return self.classifier.predict(samples)
