from sklearn import tree
import matplotlib.pyplot as plot
from .classifier import Classifier


class DecisionTreeClassifier(Classifier):

    def __init__(
            self,
            criterion='gini',
            max_depth=None,
            min_samples_split=2,
            min_samples_leaf=1,
            max_features=None
    ):
        self.classifier = tree.DecisionTreeClassifier(
            criterion=criterion,
            max_depth=max_depth,
            min_samples_split=min_samples_split,
            min_samples_leaf=min_samples_leaf,
            max_features=max_features
        )

    def get_method_name(self):
        return 'decision tree'

    def create_model(self, samples, classes):
        self.classifier = self.classifier.fit(samples, classes)

    def predict(self, samples):
        return self.classifier.predict(samples)

    def display_tree(self):
        plot.figure()
        tree.plot_tree(self.classifier, filled=True)
        plot.show()
