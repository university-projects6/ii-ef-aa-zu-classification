from abc import ABC, abstractmethod


class Classifier(ABC):

    @abstractmethod
    def create_model(self, samples, classes):
        pass

    @abstractmethod
    def predict(self, samples):
        pass

    @abstractmethod
    def get_method_name(self):
        pass
