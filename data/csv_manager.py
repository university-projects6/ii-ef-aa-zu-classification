import csv


class CSVManager:

    @staticmethod
    def read(path, filename):
        if '.csv' not in filename:
            filename += '.csv'
        full_path = path + '/' + filename
        with open(full_path, newline='') as file:
            csv_reader = csv.reader(file, delimiter=',', quotechar='"')
            rows = []
            for row in csv_reader:
                rows.append(row)
            return rows

    @staticmethod
    def write(path, filename, columns_names, rows):
        if '.csv' not in filename:
            filename += '.csv'
        full_path = path + '/' + filename
        with open(full_path, 'w', newline='') as file:
            csv_writer = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            csv_writer.writerow(columns_names)
            for row in rows:
                csv_writer.writerow(row)

    @staticmethod
    def get_row_at_index(data, row_index):
        return data[row_index]

    @staticmethod
    def get_and_remove_row_at_index(data, row_index):
        return data.pop(row_index)

    @staticmethod
    def get_column_at_index(data, column_index):
        column = []
        for row in data:
            column.append(row[column_index])
        return column

    @staticmethod
    def get_and_remove_column_at_index(data, column_index):
        column = []
        for row in data:
            column.append(row.pop(column_index))
        return column

    @staticmethod
    def split_into_partitions(data, partitions_num):
        def splitter():
            for i in range(0, partitions_num):
                yield data[i::partitions_num]
        return list(splitter())

    @staticmethod
    def convert_values_to_numeric(data):
        for row in data:
            for column_idx, column in enumerate(row):
                row[column_idx] = float(column)
        return data
