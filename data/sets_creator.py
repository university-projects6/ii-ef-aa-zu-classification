from .csv_manager import CSVManager


class SetsCreator:

    @staticmethod
    def create_sets(data, amount):
        testing_sets = CSVManager.split_into_partitions(data, amount)
        training_sets = [[] for i in range(0, amount)]
        for training_idx, training_set in enumerate(training_sets):
            for testing_idx, testing_set in enumerate(testing_sets):
                if training_idx != testing_idx:
                    training_set.extend(testing_set)
        return testing_sets, training_sets
