from runners import ClassificationRunner, ExperimentRunner
from data import CSVManager
from enums import (
    ClassificationMethodEnum,
    TestingMethodEnum,
    RunTypeEnum,
    NearestCentroidParametersEnum,
    DecisionTreeParametersEnum
)
from datetime import datetime
import numpy


# PARAMETERS
classification_method = ClassificationMethodEnum.DECISION_TREE
testing_method = TestingMethodEnum.TEN_FOLD_VALIDATION
run_type = RunTypeEnum.CLASSIFICATION_TEST
parameter_to_test = DecisionTreeParametersEnum.CRITERION

parameters = {
    ClassificationMethodEnum.DECISION_TREE: {
        DecisionTreeParametersEnum.CRITERION: ['gini'],
        DecisionTreeParametersEnum.MAX_DEPTH: [6],
        DecisionTreeParametersEnum.MIN_SAMPLES_SPLIT: [54],
        DecisionTreeParametersEnum.MIN_SAMPLES_LEAF: [27],
        DecisionTreeParametersEnum.MAX_FEATURES: [39]
    },
    ClassificationMethodEnum.NEAREST_CENTROID: {
        NearestCentroidParametersEnum.SHRINK_THRESHOLD: [0.01]  # numpy.arange(0.01, 1.01, 0.01)
    }
}

resources_path = './resources'
results_path = './results'
input_data_filename = 'data'

display_decision_tree = False
save_results_to_file = True

# READING AND PREPARING INPUT DATA
print('Reading input data...')
samples = CSVManager.read(resources_path, input_data_filename)
columns_names = CSVManager.get_and_remove_row_at_index(samples, 0)
samples = CSVManager.convert_values_to_numeric(samples)
samples_classes = CSVManager.get_and_remove_column_at_index(samples, 0)

# RUNNING THE CLASSIFICATION ALGORITHM
if run_type == RunTypeEnum.CLASSIFICATION_TEST:
    print('Running classification test for {0} classifier with testing method {1}...'.format(
        classification_method.name,
        testing_method.name
    ))
    runner = ClassificationRunner(
        classification_method,
        testing_method
    )
    params = {k: v[0] for k, v in parameters[classification_method].items()}
    accuracies, avg_accuracy = runner.test_classification(
        params,
        samples,
        samples_classes,
        display_graphics=display_decision_tree
    )
elif run_type == RunTypeEnum.EXPERIMENT:
    runner = ExperimentRunner(
        classification_method,
        testing_method
    )
    experiment_results = runner.run_experiments(
        parameters[classification_method],
        parameter_to_test,
        samples,
        samples_classes
    )

# SAVING RESULTS TO A FILE
if save_results_to_file:
    print('Saving results to a file...')
    columns = ['Classifier', 'Testing method'] + \
              [parameter.name for parameter in parameters[classification_method].keys()]
    if run_type == RunTypeEnum.CLASSIFICATION_TEST:
        columns += ['Iteration', 'Accuracy', 'Avg accuracy']
        data_to_save = []
        for idx, accuracy in enumerate(accuracies):
            data_to_save.append(
                [classification_method.name, testing_method.name] +
                [p[0] for p in parameters[classification_method].values()] +
                [idx + 1, accuracy, avg_accuracy]
            )
        file_name = 'result'
    elif run_type == RunTypeEnum.EXPERIMENT:
        columns += ['Iteration', 'Avg accuracy']
        data_to_save = []
        for idx, (tested_value, avg_accuracy) in enumerate(experiment_results):
            data_to_save.append(
                [classification_method.name, testing_method.name] +
                [
                    p_value[0] if p_key != parameter_to_test else tested_value
                    for (p_key, p_value) in parameters[classification_method].items()
                ] +
                [idx + 1, avg_accuracy]
            )
        file_name = 'experiment'
    CSVManager.write(
        results_path,
        '{0}_{1}'.format(file_name, datetime.now().strftime('%d.%m.%Y_%H.%M.%S')),
        columns,
        data_to_save
    )

print('\n\n------------------------')
print('Script finished successfully')
print('------------------------\n\n')
