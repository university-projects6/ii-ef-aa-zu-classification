from .classification_runner import ClassificationRunner


class ExperimentRunner:

    def __init__(self, classification_method, testing_method):
        self.classification_method = classification_method
        self.testing_method = testing_method

    def run_experiments(
            self,
            parameters_ranges,
            parameter_to_test,
            samples,
            samples_classes
    ):
        # prepare classification runner
        runner = ClassificationRunner(
            self.classification_method,
            self.testing_method
        )
        # iterate through range of parameters
        results = []
        params = {k: v[0] for k, v in parameters_ranges.items()}
        param_range = parameters_ranges.get(parameter_to_test, [])
        for idx, value in enumerate(param_range):
            print('Experiment iteration {0}/{1}:'.format(idx + 1, len(param_range)))
            params.update({parameter_to_test: value})
            _, avg_accuracy = runner.test_classification(
                params,
                samples,
                samples_classes
            )
            results.append((value, avg_accuracy))
            print('\nExperiment iteration average accuracy: {0}\n'.format(avg_accuracy))
        return results
