from classifiers import DecisionTreeClassifier, NearestCentroidClassifier
from enums import ClassificationMethodEnum, TestingMethodEnum, NearestCentroidParametersEnum, DecisionTreeParametersEnum
from data import SetsCreator


class ClassificationRunner:

    def __init__(self, classification_method, testing_method):
        self.classification_method = classification_method
        self.testing_method = testing_method

    def __prepare_classifier__(self, parameters):
        if self.classification_method == ClassificationMethodEnum.DECISION_TREE:
            return DecisionTreeClassifier(
                criterion=parameters.get(DecisionTreeParametersEnum.CRITERION, 'gini'),
                max_depth=parameters.get(DecisionTreeParametersEnum.MAX_DEPTH, None),
                min_samples_split=parameters.get(DecisionTreeParametersEnum.MIN_SAMPLES_SPLIT, 2),
                min_samples_leaf=parameters.get(DecisionTreeParametersEnum.MIN_SAMPLES_LEAF, 1),
                max_features=parameters.get(DecisionTreeParametersEnum.MAX_FEATURES, None)
            )
        elif self.classification_method == ClassificationMethodEnum.NEAREST_CENTROID:
            return NearestCentroidClassifier(
                shrink_threshold=parameters.get(NearestCentroidParametersEnum.SHRINK_THRESHOLD, None)
            )

    def __get_sets_num__(self, samples):
        if self.testing_method == TestingMethodEnum.CROSS_VALIDATION:
            return 2
        elif self.testing_method == TestingMethodEnum.TEN_FOLD_VALIDATION:
            return 10
        elif self.testing_method == TestingMethodEnum.LEAVE_ONE_OUT:
            return len(samples)

    @staticmethod
    def __calculate_accuracy__(predictions, expected_values):
        matches = 0
        for prediction, expected in zip(predictions, expected_values):
            if prediction == expected:
                matches += 1
        return (matches / len(predictions)) * 100

    def __classify__(
            self,
            parameters,
            training_set,
            testing_set,
            training_classes,
            testing_classes,
            display_graphics=False
    ):
        # prepare classifier
        classifier = self.__prepare_classifier__(parameters)
        # create classification model
        classifier.create_model(training_set, training_classes)
        # display graphic representation of the model
        if self.classification_method == ClassificationMethodEnum.DECISION_TREE and display_graphics:
            classifier.display_tree()
        # run the classification process
        predictions = classifier.predict(testing_set)
        # calculate accuracy and return result
        return self.__calculate_accuracy__(predictions, testing_classes)

    def test_classification(
            self,
            parameters,
            samples,
            samples_classes,
            display_graphics=False
    ):
        # prepare training and testing sets and classes
        print('Preparing data sets...')
        sets_num = self.__get_sets_num__(samples)
        testing_sets, training_sets = SetsCreator.create_sets(samples, sets_num)
        testing_sets_classes, training_sets_classes = SetsCreator.create_sets(samples_classes, sets_num)
        # iterate through sets and test classification
        accuracies = []
        for idx, training_set in enumerate(training_sets):
            print('Classification iteration {0}/{1}:'.format(idx + 1, sets_num))
            accuracy = self.__classify__(
                parameters,
                training_set,
                testing_sets[idx],
                training_sets_classes[idx],
                testing_sets_classes[idx],
                display_graphics=display_graphics
            )
            accuracies.append(accuracy)
            print('Iteration accuracy: {0}'.format(accuracy))
        # calculate average accuracy and return results
        avg_accuracy = sum(accuracies) / len(accuracies)
        print('\nClassification test finished with average accuracy: {0}\n'.format(avg_accuracy))
        return accuracies, avg_accuracy
