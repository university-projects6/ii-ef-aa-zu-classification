from enum import Enum


class DecisionTreeParametersEnum(Enum):
    CRITERION = 'criterion'
    MAX_DEPTH = 'max_depth'
    MIN_SAMPLES_SPLIT = 'min_samples_split'
    MIN_SAMPLES_LEAF = 'min_samples_leaf'
    MAX_FEATURES = 'max_features'
