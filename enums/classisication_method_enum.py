from enum import Enum


class ClassificationMethodEnum(Enum):
    DECISION_TREE = 1
    NEAREST_CENTROID = 2
