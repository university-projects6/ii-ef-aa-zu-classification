from enum import Enum


class RunTypeEnum(Enum):
    CLASSIFICATION_TEST = 1
    EXPERIMENT = 2
