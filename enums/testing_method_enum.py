from enum import Enum


class TestingMethodEnum(Enum):
    CROSS_VALIDATION = 1
    TEN_FOLD_VALIDATION = 2
    LEAVE_ONE_OUT = 3
