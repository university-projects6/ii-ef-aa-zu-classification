from enum import Enum


class NearestCentroidParametersEnum(Enum):
    SHRINK_THRESHOLD = 'shrink_threshold'
